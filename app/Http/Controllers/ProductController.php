<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use App\Models\Category;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::all();
        $categories = Category::all();
        $products = Product::all();
        return view('products.index', ['products' => $products, 'units' => $units, 'categories' => $categories]);
    }

    public function datatable()
    {
        $products = Product::all();
        return ProductResource::collection($products);
        // return response()->json(['data' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product = Product::create($request->all());

        if($request->hasFile('product_image'))
        {
            $image = $request->file('product_image');
            $fileName = time() . '.' . $image->getClientOriginalExtension();

            Image::make($image)->resize(200, 200)->save(public_path('storage/' . $fileName));

            $product->product_image = $fileName;
            $product->save();
        }

        return redirect()->route('products.index')->with('status', 'Product Created Successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $units = Unit::all();
        $categories = Category::all();
        $data = Product::find($product->id);
        return view('products.edit', ['data' => $data, 'units' => $units, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $request->merge(['id' => $product->id]);
        $product = Product::updateOrCreate(['id' => $request->id], [
            'unit_id' => $request->unit_id,
            'category_id' => $request->category_id,
            'barcode' => $request->barcode,
            'sku' => $request->sku,
            'name' => $request->name,
            'cost_price' => $request->cost_price,
            'sale_price' => $request->sale_price,
            'frequency' => $request->frequency,
            'status' => $request->status,
        ]);

        if($request->hasFile('product_image'))
        {
            $image = $request->file('product_image');
            $fileName = time() . '.' . $image->getClientOriginalExtension();

            Image::make($image)->resize(200, 200)->save(public_path('storage/' . $fileName));

            $product->product_image = $fileName;
            $product->save();
        }

            return redirect()->route('products.index')->with('status', 'Product Updated Successfuly!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}