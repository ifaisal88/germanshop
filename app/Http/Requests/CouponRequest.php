<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required|boolean',
            'value' => 'required|numeric',
            // 'from' => 'nullable|date',
            // 'to' => 'nullable|date',
            'status' => 'required|boolean',
            'description' => 'Nullable|string|max:255'
        ];
    }
}
