<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unit_id' => 'required|integer',
            'category_id' => 'required|integer',
            'barcode' => 'required',
            'sku' => 'required|string',
            'name' => ['required', 'max:50', 'min:3'],
            'cost_price' => 'required',
            'sale_price' => 'required',
            'description' => 'max:255',
            'frequency' => ['required', 'min:1', 'max:2'],
            'status' => ['required', 'max:1', 'numeric'],
            'product_image' => ['sometimes', 'mimes:png,jpg,jpeg']
        ];
    }
}
