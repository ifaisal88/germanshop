<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SellerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required|integer',
            'name' => 'required|string',
            'email' => 'required|email',
            'phone_number' => 'required|string',
            'billing_address' => 'required|string|max:255',
            'bank' => 'nullable|string',
            'account_number' => 'nullable|string',
            'iban' => 'nullable|string',
            'description' => 'nullable|string|max:255',
            'status' => 'required|boolean'
        ];
    }
}
