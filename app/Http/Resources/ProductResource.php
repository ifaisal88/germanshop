<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        'id' => isset($this->id)?$this->id:'',
        'product_image' => isset($this->product_image)?$this->product_image:'',
        'unit' => isset($this->unit->name)?$this->unit->name:'',
        'category' => isset($this->category->name)?$this->category->name:'',
        'barcode' => isset($this->barcode)?$this->barcode:'',
        'sku' => isset($this->sku)?$this->sku:'',
        'name' => isset($this->name)?$this->name:'',
        'cost_price' => isset($this->cost_price)?$this->cost_price:'',
        'sale_price' => isset($this->sale_price)?$this->sale_price:'',
        'description' => isset($this->description)?$this->description:'',
        'frequency' => isset($this->frequency)?$this->frequency:'',
        'status' => ($this->status == '1') ? 'Active' : 'In-Active',
        ];
        // return parent::toArray($request);
    }
}
