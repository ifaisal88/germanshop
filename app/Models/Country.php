<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'status',
    ];

    /**
     * Get all of the seller for the Country
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function seller()
    {
        return $this->hasMany(Seller::class);
    }
}