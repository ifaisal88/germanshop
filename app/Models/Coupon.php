<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
        'value',
        'from',
        'to',
        'status',
        'description',
        'created_at',
        'updated_at',
        'deleated_at'
    ];
}
