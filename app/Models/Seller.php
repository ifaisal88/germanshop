<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;

    protected $fillable = [
        'country_id',
        'name',
        'email',
        'phone_number',
        'billing_address',
        'bank',
        'account_number',
        'iban',
        'description',
        'status',
        'created_at',
        'updated_at',
        'deleated_at'
    ];

    /**
     * Get the country that owns the Seller
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}