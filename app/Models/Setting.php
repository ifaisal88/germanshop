<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'url',
        'email',
        'phone',
        'fax',
        'currency_symbol',
        'return_policy',
        'logo',
        'banner1',
        'banner2',
        'banner3',
        'banner4',
        'banner5',
        'created_at',
        'updated_at'
    ];
}
