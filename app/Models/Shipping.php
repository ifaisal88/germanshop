<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'zip_code',
        'minimum_value',
        'shipping_charges',
        'status',
        'description',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}