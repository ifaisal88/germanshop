<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('category_id');
            $table->string('barcode')->nullable()->default(null);
            $table->string('sku')->nullable()->default(null);
            $table->string('name')->unique();
            $table->float('cost_price', 8, 2);
            $table->float('sale_price', 8, 2);
            $table->string('description')->nullable()->default(null);
            $table->tinyInteger('frequency');
            $table->string('product_image');
            $table->boolean('status')->nullable()->default(true);
            $table->timestamps();

            $table->foreign('unit_id')
                  ->references('id')
                  ->on('units');

            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
