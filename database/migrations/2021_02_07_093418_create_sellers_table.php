<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->id();
            $table->foreignID('country_id')->constrained();
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('phone_number')->nullable()->default(null);
            $table->string('billing_address')->nullable()->default(null);
            $table->string('bank')->nullable()->default(null);
            $table->string('account_number')->nullable()->default(null);
            $table->string('iban')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->boolean('status')->required()->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
