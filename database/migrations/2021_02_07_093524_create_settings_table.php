<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('name')->required()->default(null);
            $table->string('address')->required()->default(null);
            $table->string('url')->required()->default(null);
            $table->string('email')->required()->default(null);
            $table->string('phone')->required()->default(null);
            $table->string('fax')->required()->default(null);
            $table->string('return_policy')->required()->default(null);
            $table->string('currency_symbol')->required()->default(null);
            $table->string('logo')->required()->default(null);
            $table->string('banner1')->nullable()->default(null);
            $table->string('banner2')->nullable()->default(null);
            $table->string('banner3')->nullable()->default(null);
            $table->string('banner4')->nullable()->default(null);
            $table->string('banner5')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}