<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();
        if (count($categories)==0) {
            DB::table('categories')->insert([
                [
                    'name' => 'Snacks',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Soft Drinks',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Biscuits',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
