<?php

namespace Database\Seeders;


use Carbon\Carbon;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Country::all();
        if (count($countries)==0) {
            DB::table('countries')->insert([
                [
                    'name' => 'Germany',
                    'description' => '',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Finland',
                    'description' => '',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Greenland',
                    'description' => '',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
