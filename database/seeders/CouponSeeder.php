<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Coupon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coupons = Coupon::all();
        if (count($coupons)==0) {
            DB::table('coupons')->insert([
                [
                    'name' => 'Coupon 1',
                    'type' => '0',
                    'value' => '2.99',
                    'from' => new Carbon(),
                    'to' =>  new Carbon(),
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Coupon 2',
                    'type' => '1',
                    'value' => '3.99',
                    'from' => new Carbon(),
                    'to' =>  new Carbon(),
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Coupon 3',
                    'type' => '1',
                    'value' => '2.09',
                    'from' => new Carbon(),
                    'to' =>  new Carbon(),
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
