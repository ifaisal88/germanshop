<?php

namespace Database\Seeders;

use App\Models\Unit;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
          \App\Models\User::factory(10)->create();
         $this->call([         
            UnitSeeder::class,
            CategorySeeder::class,
            ProductSeeder::class,
            CountrySeeder::class,
            SellerSeeder::class,
            ShippingSeeder::class,
            UnitSeeder::class,
            SettingSeeder::class
         ]);
    }
}
