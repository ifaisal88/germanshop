<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Unit;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();        
        if (count($products)==0) {
            $unit = Unit::where('name', 'Bottle')->first();
            $category = Category::where('name', 'Snacks')->first();
            DB::table('products')->insert([
                [
                    'unit_id' => $unit->id,
                    'category_id' => $category->id,
                    'barcode' => '6001069206581',
                    'name' => 'Lays',
                    'cost_price' => '10.3',
                    'sale_price' => '15.5',
                    'description' => 'lays 125 gram(s)',
                    'frequency' => '10',
                    'product_image' => 'lays.png',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
            ]);
        } else {
            $this->command->line('Already Seeded');
        }
    }
}
