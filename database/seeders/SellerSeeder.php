<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Seller;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sellers = Seller::all();
        if (count($sellers)==0) {
            $country = Country::first();
            DB::table('sellers')->insert([
                [
                    'name' => 'Seller 1',
                    'email' => 'seller1@example.com',
                    'phone_number' => '+13455025568',
                    'billing_address' => 'Seller 1, billing address',
                    'bank' => 'Seller 1 bank Name',
                    'account_number' => 'Seller 1 Account Number',
                    'iban' => 'Seller 1 IBAN Number',
                    'country_id' => $country->id,
                    'description' => '',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Seller 2',
                    'email' => 'seller2@example.com',
                    'phone_number' => '+13455025568',
                    'address' => 'Seller 2, billing address',
                    'bank' => 'Seller 12 bank Name',
                    'account_number' => 'Seller 2 Account Number',
                    'iban' => 'Seller 2 IBAN Number',
                    'country_id' => $country->id,
                    'description' => '',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Seller 3',
                    'email' => 'seller3@example.com',
                    'phone_number' => '+13455025568',
                    'address' => 'Seller 3, billing address',
                    'bank' => 'Seller 3 bank Name',
                    'account_number' => 'Seller 3 Account Number',
                    'iban' => 'Seller 3 IBAN Number',
                    'country_id' => $country->id,
                    'description' => '',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
