<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = Setting::all();
        if (count($settings)==0) {
            DB::table('settings')->insert([
                [
                    'name' => 'Shop Name',
                    'address' => 'Shop Address',
                    'url' => 'www.example.com',
                    'email' => 'email@example.com',
                    'phone' => '+13459567722',
                    'fax' => '+13459567722',
                    'return_policy' => 'This is our return policy',
                    'currency_symbol' => 'US$',
                    'logo' => 'logo',
                    'banner1' => 'banner1',
                    'banner2' => 'banner2',
                    'banner3' => 'banner3',
                    'banner4' => 'banner4',
                    'banner5' => 'banner5',
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
