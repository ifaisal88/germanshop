<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Shipping;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShippingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shippings = Shipping::all();
        if (count($shippings)==0) {
            DB::table('shippings')->insert([
                [
                    'name' => 'Shipping 1',
                    'zip_code' => '22356',
                    'minimum_value' => '5.99',
                    'shipping_charges' => '2.99',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Shipping 2',
                    'zip_code' => '22357',
                    'minimum_value' => '5.99',
                    'shipping_charges' => '2.99',
                    'description' => '',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Shipping 3',
                    'zip_code' => '22358',
                    'minimum_value' => '5.99',
                    'shipping_charges' => '2.99',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
