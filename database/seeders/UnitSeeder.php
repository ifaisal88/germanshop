<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Unit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $units = Unit::all();
        if (count($units)==0) {
            DB::table('units')->insert([
                [
                    'name' => 'Packet',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Bag',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
                [
                    'name' => 'Bottle',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon()
                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
