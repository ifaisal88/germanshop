<x-header></x-header>

<body>
   
    <x-preloader></x-preloader>
    
    <div id="main-wrapper">
        <x-logo></x-logo>

        <x-top-nav></x-top-nav>

        <x-side-nav></x-side-nav>
		
        <div class="content-body">
			<div class="container-fluid">
                <div class="form-head d-md-flex mb-sm-4 mb-3 align-items-start">
					<div class="mr-auto  d-lg-block">
						<h2 class="text-black font-w600">Product Categories</h2>
						<p class="mb-0">Welcome to Categories Management Section</p>
					</div>
					<a href="javascript:void(0);" class="btn btn-primary rounded"><i class="flaticon-381-settings-2 mr-0"></i></a>
				</div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Add Product Category</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="{{ route('categories.update', [$data->id]) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" id="name" value="{{ old('name', $data->name) }}" class="form-control">
                                                @error('name')
                                                    <span class="formError">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>                                        
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Details</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="description" id="description" rows="4" id="comment">{{ old('name', $data->description) }}</textarea>
                                                @error('details')
                                                    <span class="formError">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Status</label>
                                            <div class="col-sm-9">
                                                <select name="status" id="status" class="form-control">
                                                    <option value="" selected>Select Status...</option>
                                                    <option value="1" {{ '1' == old('status' ? 'selected' : '') }}>Active</option>
                                                    <option value="0" {{ '0' == old('status' ? 'selected' : '') }}>In-Active</option>
                                                </select>
                                                @error('status')
                                                    <span class="formError">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn btn-primary">Update Category</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
					</div>
                    
                </div>
            </div>
        </div>
	<x-footer></x-footer>