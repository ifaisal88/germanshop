
	    <div class="footer">
			<div class="copyright">
				<p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Loha & Co.</a> 2020</p>
			</div>
		</div>
    </div>
	  <script src="{{ asset('/vendor/global/global.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/chart.js/Chart.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/apexchart/apexchart.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/owl-carousel/owl.carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/jqvmap/js/jquery.vmap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/jqvmap/js/jquery.vmap.world.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/peity/jquery.peity.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/dashboard/dashboard-1.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/deznav-init.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/vendor/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/plugins-init/datatables.init.js') }}" type="text/javascript"></script>
    
    <script src="{{ asset('/vendor/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/plugins-init/bs-daterange-picker-init.js') }}" type="text/javascript"></script>
 	</body>
</html>