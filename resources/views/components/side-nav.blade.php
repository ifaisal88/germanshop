<div class="deznav">
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            <li>
                <a class="has-arrow ai-icon" href="{{ route('dashboard') }}" aria-expanded="false">
                    <i class="icon-dashboard"></i><span class="nav-text">Dashboard</span>
                </a>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon-barcode"></i></i>Products</a>
                <ul aria-expanded="false">
                    <li>
                        <a href="{{ route('units.index') }}">
                            <i class="icon-book"></i><span class="nav-text">Units</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('categories.index') }}">
                            <i class="icon-sitemap"></i><span class="nav-text">Categories</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('products.index') }}">
                            <i class="icon-barcode"></i><span class="nav-text">Products</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('inventories.index') }}">
                            <i class="icon-archive"></i><span class="nav-text">Inventory</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow ai-icon" href="{{ route('sellers.index') }}" aria-expanded="false">
                    <i class="icon-user"></i><span class="nav-text">Sellers</span>
                </a>
            </li>
            <li>
                <a class="has-arrow ai-icon" href="{{ route('customers.index') }}" aria-expanded="false">
                    <i class="icon-group"></i><span class="nav-text">Customers</span>
                </a>
            </li>
            <li>
                <a class="has-arrow ai-icon" href="{{ route('orders.index') }}" aria-expanded="false">
                    <i class="icon-dropbox"></i><span class="nav-text">Orders</span>
                </a>
            </li>
            <li>
                <a class="has-arrow ai-icon" href="{{ route('coupons.index') }}" aria-expanded="false">
                    <i class="icon-gift"></i><span class="nav-text">Coupons</span>
                </a>
            </li>
            <li>
                <a class="has-arrow ai-icon" href="{{ route('shipping.index') }}" aria-expanded="false">
                    <i class="icon-truck"></i><span class="nav-text">Shipping</span>
                </a>
            </li>
        </ul>
    </div>
</div>