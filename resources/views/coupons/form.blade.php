<div class="form-group row">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ old('name','') }}" class="form-control">
        @error('name')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Type</label>
    <div class="col-sm-9">
        <select name="type" id="type" class="form-control">
            <option selected>Select Status...</option>
            <option value="1">Percentage</option>
            <option value="0">Fixed Amount</option>
        </select>
        @error('type')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Value</label>
    <div class="col-sm-9">
        <input type="text" name="value" id="value" value="{{ old('value','') }}" class="form-control">
        @error('value')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Remarks</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="description" id="desctiption" rows="4" id="comment"> {{ old('description','') }}</textarea>
        @error('description')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Status</label>
    <div class="col-sm-9">
        <select name="status" id="status" class="form-control">
            <option selected>Select Status...</option>
            <option value="1">Active</option>
            <option value="0">In-Active</option>
        </select>
        @error('status')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>