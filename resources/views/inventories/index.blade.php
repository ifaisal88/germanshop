<x-header></x-header>

<body>
   
    <x-preloader></x-preloader>
    
    <div id="main-wrapper">
        <x-logo></x-logo>

        <x-top-nav></x-top-nav>

        <x-side-nav></x-side-nav>
		
        <div class="content-body">
			<div class="container-fluid">
                <div class="form-head d-md-flex mb-sm-4 mb-3 align-items-start">
					<div class="mr-auto  d-lg-block">
						<h2 class="text-black font-w600">Products Inventory</h2>
						<p class="mb-0">Welcome to Inventory Section</p>
					</div>
					<x-page-settings></x-page-settings>
				</div>
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Products Inventory</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Unit</th>
                                                <th>Category</th>
                                                <th>Barcode</th>
                                                <th>Name</th>
                                                <th>Cost Price</th>
                                                <th>Sale Price</th>
                                                <th>Frequency</th>
                                                <th>Status</th>
                                                <th>Quantities</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('images/uploads/lays.png') }}" height="50" width="auto" /></td>
                                                <td>Snacks</td>
                                                <td>Bag</td>
                                                <td>1223568952419</td>
                                                <td>Lays</td>
                                                <td>3.99</td>
                                                <td>4.25</td>
                                                <td>10</td>
                                                <td>Active</td>
                                                <td>93</td>
                                            </tr>
                                        <tfoot>
                                            <tr>
                                                <th>Image</th>
                                                <th>Unit</th>
                                                <th>Category</th>
                                                <th>Barcode</th>
                                                <th>Name</th>
                                                <th>Cost Price</th>
                                                <th>Sale Price</th>
                                                <th>Frequency</th>
                                                <th>Status</th>
                                                <th>Quantities</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<x-footer></x-footer>