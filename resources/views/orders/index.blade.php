<x-header></x-header>

<body>
   
    <x-preloader></x-preloader>
    
    <div id="main-wrapper">
        <x-logo></x-logo>

        <x-top-nav></x-top-nav>

        <x-side-nav></x-side-nav>
		
        <div class="content-body">
			<div class="container-fluid">
                <div class="form-head d-md-flex mb-sm-4 mb-3 align-items-start">
					<div class="mr-auto  d-lg-block">
						<h2 class="text-black font-w600">Orders</h2>
						<p class="mb-0">Welcome to Orders management</p>
					</div>
					<x-page-settings></x-page-settings>
				</div>
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Orders</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
                                        <thead>
                                            <tr>
                                                <th>Order#</th>
                                                <th>Client</th>
                                                <th>Contact No</th>
                                                <th>Ship To</th>
                                                <th>Amount</th>
                                                <th>Details</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>#12883</td>
                                                <td>John Doe</td>
                                                <td>0341 37 52 59</td>
                                                <td>Prenzlauer Allee 19, Leipzig, Freistaat Sachsen 04254</td>
                                                <td>12.75</td>
                                                <td><button type="button" class="btn btn-primary mb-2 btn-xs" data-toggle="modal" data-target=".bd-example-modal-lg">Details</button></td>
                                                <td><span class="badge badge-success">Completed<span class="ml-1 fa fa-check"></span></span></td>
                                                <td> 
                                                    <div class="dropdown text-sans-serif"><button class="btn btn-primary tp-btn-light sharp" type="button" id="order-dropdown-0" data-toggle="dropdown" data-boundary="viewport" aria-haspopup="true" aria-expanded="false"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg></span></button>
                                                        <div class="dropdown-menu dropdown-menu-right border py-0" aria-labelledby="order-dropdown-0">
                                                            <div class="py-2"><a class="dropdown-item" href="#!">Completed</a><a class="dropdown-item" href="#!">Processing</a><a class="dropdown-item" href="#!">On Hold</a><a class="dropdown-item" href="#!">Pending</a>
                                                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#!">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>#12884</td>
                                                <td>John Raven</td>
                                                <td>0342 38 42 65</td>
                                                <td>Prenzlauer Allee 19, Leipzig, Freistaat Sachsen 04254</td>
                                                <td>12.75</td>
                                                <td><button type="button" class="btn btn-primary mb-2 btn-xs" data-toggle="modal" data-target=".bd-example-modal-lg">Details</button></td>
                                                <td><span class="badge badge-primary">Processing<span class="ml-1 fa fa-redo"></span></span></td>
                                                <td> 
                                                    <div class="dropdown text-sans-serif"><button class="btn btn-primary tp-btn-light sharp" type="button" id="order-dropdown-0" data-toggle="dropdown" data-boundary="viewport" aria-haspopup="true" aria-expanded="false"><span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg></span></button>
                                                        <div class="dropdown-menu dropdown-menu-right border py-0" aria-labelledby="order-dropdown-0">
                                                            <div class="py-2"><a class="dropdown-item" href="#!">Completed</a><a class="dropdown-item" href="#!">Processing</a><a class="dropdown-item" href="#!">On Hold</a><a class="dropdown-item" href="#!">Pending</a>
                                                                <div class="dropdown-divider"></div><a class="dropdown-item text-danger" href="#!">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <tfoot>
                                            <tr>
                                                <th>Order#</th>
                                                <th>Client</th>
                                                <th>Contact No</th>
                                                <th>Ship To</th>
                                                <th>Amount</th>
                                                <th>Details</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Order Details</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table id="example" class="table table-striped">
                                                            <thead>
                                                                <tr>   
                                                                    <th>Sr.</th>
                                                                    <th>Name</th>
                                                                    <th>Barcode</th>
                                                                    <th>Qty</th>
                                                                    <th>Price</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>Lays</td>
                                                                    <td>1223568952419</td>
                                                                    <td>2</td>
                                                                    <td>4.25</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>Lays</td>
                                                                    <td>1223568952419</td>
                                                                    <td>2</td>
                                                                    <td>4.25</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>Lays</td>
                                                                    <td>1223568952419</td>
                                                                    <td>2</td>
                                                                    <td>4.25</td>
                                                                </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <strong>Total Order Price: 12.75</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<x-footer></x-footer>

    