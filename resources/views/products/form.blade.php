<div class="form-group row">
    <label class="col-sm-3 col-form-label">Unit</label>
    <div class="col-sm-9">
        <select name="unit_id" id="unit_id" class="form-control">
            <option value="" selected>Select Unit...</option>
            @foreach ($units as $unit)
                <option value="{{ $unit->id }}" {{ old('unit_id') == $unit->id ? "selected" : "" }}>{{ $unit->name }}</option>
            @endforeach
        </select>
        @error('unit_id')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Category</label>
    <div class="col-sm-9">
        <select name="category_id" id="category_id" class="form-control">
            <option value="" selected>Select Category...</option>
            @foreach ($categories as $category)
                <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? "selected" : "" }}>{{ $category->name }}</option>
            @endforeach
        </select>
        @error('category_id')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>                                        
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Barcode</label>
    <div class="col-sm-9">
        <input name="barcode" id="barcode" type="text" value="{{ old('barcode', '') }}" class="form-control">
        @error('barcode')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">SKU</label>
    <div class="col-sm-9">
        <input name="sku" id="sku" type="text" value="{{ old('sku', '') }}" class="form-control">
        @error('sku')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div> 
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        <input name="name" id="name" type="text" value="{{ old('name', '') }}" class="form-control">
        @error('name')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>   
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Cost Price</label>
    <div class="col-sm-9">
        <input name="cost_price" id="cost_price" type="text" value="{{ old('cost_price', '') }}" class="form-control">
        @error('cost_price')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>   
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Sale Price</label>
    <div class="col-sm-9">
        <input name="sale_price" id="sale_price" type="text" value="{{ old('sale_price', '') }}" class="form-control">
        @error('sale_price')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>                                        
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Details</label>
    <div class="col-sm-9">
        <textarea name="description" id="description" class="form-control" rows="4" id="comment"> {{ old('description', '') }}</textarea>
        @error('description')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Frequency</label>
    <div class="col-sm-9">
        <input name="frequency" id="frequency" type="text" value="{{ old('frequency', '') }}" class="form-control">
        @error('frequency')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div> 
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Status</label>
    <div class="col-sm-9">
        <select name="status" id="status" class="form-control">
            <option selected>Select Status...</option>
            <option value="1">Active</option>
            <option value="0">In-Active</option>
        </select>
        @error('status')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="custom-file">
        <label class="col-sm-3 custom-file-label">Choose file</label>
        <div class="col-sm-9">
            <input name="product_image" id="product_image" type="file" class="form-control">
            @error('product_image')
                <span class="formError">{{ $message }}</span>
            @enderror
        </div>
    </div>
</div>