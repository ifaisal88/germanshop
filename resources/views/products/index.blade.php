<x-header></x-header>

<body>
   
    <x-preloader></x-preloader>
    
    <div id="main-wrapper">
        <x-logo></x-logo>

        <x-top-nav></x-top-nav>

        <x-side-nav></x-side-nav>
		
        <div class="content-body">
			<div class="container-fluid">
                <div class="form-head d-md-flex mb-sm-4 mb-3 align-items-start">
					<div class="mr-auto  d-lg-block">
						<h2 class="text-black font-w600">Products</h2>
						<p class="mb-0">Welcome to Products Management Section</p>
					</div>
					<x-page-settings></x-page-settings>
				</div>
				<div class="row">
                    <div class="col-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Add Products</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        @include('products.form')
                                        <div class="form-group row">
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn btn-primary">Add Product</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
					</div>
                    <div class="col-8">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">All Products</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Unit</th>
                                                <th>Category</th>
                                                <th>Barcode</th>
                                                <th>SKU</th>
                                                <th>Name</th>
                                                <th>Cost Price</th>
                                                <th>Sale Price</th>
                                                <th>Frequency</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($products as $product)
                                            <tr>
                                                <td><img src="{{ asset('storage/' . $product->product_image) }}" height="50" width="auto" /></td>
                                                <td>{{ $product->unit->name }}</td>
                                                <td>{{ $product->category->name }}</td>
                                                <td>{{ $product->barcode }}</td>
                                                <td>{{ $product->sku }}</td>
                                                <td>{{ $product->name }}</td>
                                                <td>{{ $product->cost_price }}</td>
                                                <td>{{ $product->sale_price }}</td>
                                                <td>{{ $product->frequency }}</td>
                                                <td>{{ ($product->status == 1) ? 'Active' : 'In-Active' }}</td>
                                                <td><a type="button" class="btn btn-primary mb-2 btn-xs" href="{{ route('products.edit', [$product->id]) }}">Update</a></td>
                                            </tr>
                                            @endforeach
                                        <tfoot>
                                            <tr>
                                                <th>Image</th>
                                                <th>Unit</th>
                                                <th>Category</th>
                                                <th>Barcode</th>
                                                <th>SKU</th>
                                                <th>Name</th>
                                                <th>Cost Price</th>
                                                <th>Sale Price</th>
                                                <th>Frequency</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <div class="modal fade" id="editDetails">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edit Unit</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="basic-form">
                                                        <form>
                                                            @csrf
                                                            @include('products.form')
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger light" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<x-footer></x-footer>