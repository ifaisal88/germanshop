<div class="form-group row">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        <input name="name" id="name" type="text" value="{{ old('name', '') }}" class="form-control">
        @error('name')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>   
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Email</label>
    <div class="col-sm-9">
        <input name="email" id="email" type="text" value="{{ old('email', '') }}" class="form-control">
        @error('email')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>   
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Phone Number</label>
    <div class="col-sm-9">
        <input name="phone_number" id="phone_number" value="{{ old('phone_number', '') }}" type="text" class="form-control">
        @error('phone_number')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Billing Address</label>
    <div class="col-sm-9">
        <textarea name="billing_address" id="billing_address" class="form-control" rows="4" id="comment">{{ old('billing_address', '') }}</textarea>
        @error('billing_address')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Country</label>
    <div class="col-sm-9">
        <select name="country_id" id="country_id" class="form-control">
            <option value="" selected>Select Country...</option>
            @foreach ($countries as $country)
                <option value="{{ $country->id }}" {{ old('country_id') == $country->id ? "selected" : "" }}>{{ $country->name }}</option>
            @endforeach
        </select>
        @error('country_id')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Bank</label>
    <div class="col-sm-9">
        <input name="bank" id="bank" type="text" value="{{ old('bank', '') }}" class="form-control">
        @error('bank')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Account Number</label>
    <div class="col-sm-9">
        <input name="account_number" id="account_number" value="{{ old('account_number', '') }}" type="text" class="form-control">
        @error('account_number')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">IBAN Number</label>
    <div class="col-sm-9">
        <input name="iban" id="iban" type="text" value="{{ old('iban', '') }}" class="form-control">
        @error('iban')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Remarks</label>
    <div class="col-sm-9">
        <textarea name="description" id="description" class="form-control" rows="4" id="comment">{{ old('description', '') }}</textarea>
        @error('description')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Status</label>
    <div class="col-sm-9">
        <select name="status" id="status" class="form-control">
            <option selected>Select Status...</option>
            <option value="1">Active</option>
            <option value="0">In-Active</option>
        </select>
        @error('status')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>