<x-header></x-header>

<body>
   
    <x-preloader></x-preloader>
    
    <div id="main-wrapper">
        <x-logo></x-logo>

        <x-top-nav></x-top-nav>

        <x-side-nav></x-side-nav>
		
        <div class="content-body">
			<div class="container-fluid">
                <div class="form-head d-md-flex mb-sm-4 mb-3 align-items-start">
					<div class="mr-auto  d-lg-block">
						<h2 class="text-black font-w600">Sellers</h2>
						<p class="mb-0">Welcome to Sellers Management Section</p>
					</div>
					<x-page-settings></x-page-settings>
				</div>
				<div class="row">
                    <div class="col-4">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Add Seller</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="{{ route('sellers.store') }}" method="POST">
                                        @csrf
                                        @include('sellers.form')
                                        <div class="form-group row">
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn btn-primary">Add Seller</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
					</div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example" class="display min-w850">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone Number</th>
                                                <th>Billing Address </th>
                                                <th>bank</th>
                                                <th>Account Number</th>
                                                <th>IBAN</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="customers">
                                            @forelse ($sellers as $seller)
                                                <tr>
                                                    <td>{{ $seller->name }}</td>
                                                    <td><a href="mailto:{{ $seller->email }}">{{ $seller->email }}</a></td>
                                                    <td><a href="tel:{{ $seller->phone_number }}">{{ $seller->phone_number }}</td>
                                                    <td>{{ $seller->billing_address . ', ' . $seller->country->name }}</td>
                                                    <td>{{ $seller->bank }}</td>
                                                    <td>{{ $seller->account_number }}</td>
                                                    <td>{{ $seller->iban }}</td>
                                                    <td>{{ $seller->description }}</td>
                                                    <td>{{ ($seller->status == '1') ? 'Active' : 'In_Active' }}</td>
                                                    <td><button type="button" class="btn btn-primary mb-2 btn-xs" data-toggle="modal" data-target="#editDetails">Update</button></td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="10">No Data Found</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<x-footer></x-footer>