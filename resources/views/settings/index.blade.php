<x-header></x-header>

<body>
   
    <x-preloader></x-preloader>
    
    <div id="main-wrapper">
        <x-logo></x-logo>

        <x-top-nav></x-top-nav>

        <x-side-nav></x-side-nav>
		
        <div class="content-body">
			<div class="container-fluid">
                <div class="form-head d-md-flex mb-sm-4 mb-3 align-items-start">
					<div class="mr-auto  d-lg-block">
						<h2 class="text-black font-w600">Shop Settings</h2>
						<p class="mb-0">Welcome to Site Settings Section</p>
					</div>
				</div>
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Shop Settings</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="{{ route('settings.update', $data->id) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">Shop Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" id="name" value="{{ old('name', $data->name) }}" class="form-control">
                                                @error('name')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">Address</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="address" id="address" rows="4">{{ old('address', $data->address) }}</textarea>
                                                @error('address')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">URL</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="url" id="url" value="{{ old('url', $data->url) }}" class="form-control">
                                                @error('url')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="email" id="email" value="{{ old('email', $data->email) }}" class="form-control">
                                                @error('email')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">Phone</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="phone" id="phone" value="{{ old('phone', $data->phone) }}" class="form-control">
                                                @error('phone')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">Fax</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="fax" id="fax" value="{{ old('fax', $data->fax) }}" class="form-control">
                                                @error('fax')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">Return Policy</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="return_policy" id="return_policy" rows="4"">{{ old('return_policy', $data->return_policy) }}</textarea>
                                                @error('return_policy')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-sm-3 col-form-label">Currency Symbol</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="currency_symbol" id="currency_symbol" value="{{ old('currency_symbol', $data->currency_symbol) }}" class="form-control">
                                                @error('currency_symbol')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <div class="custom-file">
                                                <label class="custom-file-label">Logo</label>
                                                <input type="file" name="logo" id="logo" value="{{ old('logo', $data->logo) }}" class="custom-file-input">
                                                @error('logo')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <div class="custom-file">
                                                <label class="custom-file-label">Banner 1</label>
                                                <input type="file" name="banner1" id="banner1" value="{{ old('banner1', $data->banner1) }}" class="custom-file-input">
                                                @error('banner1')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <div class="custom-file">
                                                <label class="custom-file-label">Banner 2</label>
                                                <input type="file" name="banner2" id="banner2" value="{{ old('banner2', $data->banner2) }}" class="custom-file-input">
                                                @error('banner2')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <div class="custom-file">
                                                <label class="custom-file-label">Banner 3</label>
                                                <input type="file" name="banner3" id="banner3" value="{{ old('banner3', $data->banner3) }}" class="custom-file-input">
                                                @error('banner3')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <div class="custom-file">
                                                <label class="custom-file-label">Banner 4</label>
                                                <input type="file" name="banner4" id="banner4" value="{{ old('banner4', $data->banner4) }}" class="custom-file-input">
                                                @error('banner4')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <div class="custom-file">
                                                <label class="custom-file-label">Banner 5</label>
                                                <input type="file" name="banner5" id="banner5" value="{{ old('banner5', $data->banner5) }}" class="custom-file-input">
                                                @error('banner5')
                                                    <span class="formError">{{ $message }}</span>    
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn btn-primary">Update Settings</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
					</div>
                </div>
            </div>
        </div>
	<x-footer></x-footer>