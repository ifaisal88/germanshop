<div class="form-group row">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ old('name', '') }}" class="form-control">
        @error('name')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>                                        
<div class="form-group row">
    <label class="col-sm-3 col-form-label">ZIP Code</label>
    <div class="col-sm-9">
        <input type="text" name="zip_code" id="zip_code" value="{{ old('zip_code', '') }}" class="form-control">
        @error('zip_code')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>                                        
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Min Price</label>
    <div class="col-sm-9">
        <input type="text" name="minimum_value" id="minimum_value" value="{{ old('minimum_value', '') }}" class="form-control">
        @error('minimum_value')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>                                        
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Charges</label>
    <div class="col-sm-9">
        <input type="text" name="shipping_charges" id="shipping_charges"  value="{{ old('shipping_charges', '') }}" class="form-control">
        @error('shipping_charges')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Remarks</label>
    <div class="col-sm-9">
        <textarea name="description" id="description" class="form-control" rows="4" id="comment">{{ old('description', '') }}</textarea>
        @error('description')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Status</label>
    <div class="col-sm-9">
        <select name="status" id="status" class="form-control">
            <option selected>Select Status...</option>
            <option value="1">Active</option>
            <option value="0">In-Active</option>
        </select>
        @error('status')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>