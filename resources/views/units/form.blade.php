<div class="form-group row">
    <label class="col-sm-3 col-form-label">Name</label>
    <div class="col-sm-9">
        <input type="text" name="name" id="name" class="form-control" {{ old('name', '') }}>
        @error('name')
        <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>                                        
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Details</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="description" id="description" rows="4" id="comment">{{ old('description', '') }}</textarea>
        @error('details')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 col-form-label">Status</label>
    <div class="col-sm-9">
        <select name="status" id="status" class="form-control">
            <option value="" selected>Select Status...</option>
            <option value="1">Active</option>
            <option value="0">In-Active</option>
        </select>
        @error('status')
            <span class="formError">{{ $message }}</span>
        @enderror
    </div>
</div>