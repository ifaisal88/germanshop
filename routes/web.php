<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\ShippingController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::middleware(['auth'])->group( function(){
    Route::get('/', function () {return view('dashboard');});
	Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resources([
        'categories' => CategoryController::class,
        'customers' => CustomerController::class,
        'inventories' => InventoryController::class,
        'orders' => OrderController::class,
        'reports' => ReportController::class,
        'units' => UnitController::class,
    ]);

        Route::get('/products/data-table', [ProductController::class, 'datatable'])->name('dataTable');
        Route::get('/products/list', [ProductController::class, 'index'])->name('products.index');
        Route::post('/products/store', [ProductController::class, 'store'])->name('products.store');
        Route::get('/products/{product}/edit', [ProductController::class, 'edit'])->name('products.edit');
        Route::put('/products/{product}/update', [ProductController::class, 'update'])->name('products.update');
        Route::post('/sellers/store', [SellerController::class, 'store'])->name('sellers.store');
        Route::get('/sellers', [SellerController::class, 'index'])->name('sellers.index');
        Route::get('/coupons', [CouponController::class, 'index'])->name('coupons.index');
        Route::get('/settings', [SettingController::class, 'index'])->name('settings.index');
        Route::put('/settings/{setting}/update-settings', [SettingController::class, 'update'])->name('settings.update');
        Route::post('/coupons/store', [CouponController::class, 'store'])->name('coupons.store');
        Route::get('/shipping', [ShippingController::class, 'index'])->name('shipping.index');
        Route::post('/shipping/store', [ShippingController::class, 'store'])->name('shipping.store');
});